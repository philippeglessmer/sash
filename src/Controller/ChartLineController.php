<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ChartLineController extends AbstractController
{
    #[Route('/chart-line', name: 'app_chart_line')]
    public function index(): Response
    {
        return $this->render('chart_line/index.html.twig', [
            'controller_name' => 'ChartLineController',
        ]);
    }
}
