<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RangesliderController extends AbstractController
{
    #[Route('/rangeslider', name: 'app_rangeslider')]
    public function index(): Response
    {
        return $this->render('rangeslider/index.html.twig', [
            'controller_name' => 'RangesliderController',
        ]);
    }
}
