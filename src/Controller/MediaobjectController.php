<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MediaobjectController extends AbstractController
{
    #[Route('/mediaobject', name: 'app_mediaobject')]
    public function index(): Response
    {
        return $this->render('mediaobject/index.html.twig', [
            'controller_name' => 'MediaobjectController',
        ]);
    }
}
