<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AvatarsquareController extends AbstractController
{
    #[Route('/avatarsquare', name: 'app_avatarsquare')]
    public function index(): Response
    {
        return $this->render('avatarsquare/index.html.twig', [
            'controller_name' => 'AvatarsquareController',
        ]);
    }
}
