<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ShopDescriptionController extends AbstractController
{
    #[Route('/shop-description', name: 'app_shop_description')]
    public function index(): Response
    {
        return $this->render('shop_description/index.html.twig', [
            'controller_name' => 'ShopDescriptionController',
        ]);
    }
}
