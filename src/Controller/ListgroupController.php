<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListgroupController extends AbstractController
{
    #[Route('/listgroup', name: 'app_listgroup')]
    public function index(): Response
    {
        return $this->render('listgroup/index.html.twig', [
            'controller_name' => 'ListgroupController',
        ]);
    }
}
