<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EmailComposeController extends AbstractController
{
    #[Route('/email-compose', name: 'app_email_compose')]
    public function index(): Response
    {
        return $this->render('email_compose/index.html.twig', [
            'controller_name' => 'EmailComposeController',
        ]);
    }
}
