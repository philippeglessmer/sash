<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ScrollspyController extends AbstractController
{
    #[Route('/scrollspy', name: 'app_scrollspy')]
    public function index(): Response
    {
        return $this->render('scrollspy/index.html.twig', [
            'controller_name' => 'ScrollspyController',
        ]);
    }
}
