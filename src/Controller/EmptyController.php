<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EmptyController extends AbstractController
{
    #[Route('/empty', name: 'app_empty')]
    public function index(): Response
    {
        return $this->render('empty/index.html.twig', [
            'controller_name' => 'EmptyController',
        ]);
    }
}
