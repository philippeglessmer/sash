<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EmailReadController extends AbstractController
{
    #[Route('/email-read', name: 'app_email_read')]
    public function index(): Response
    {
        return $this->render('email_read/index.html.twig', [
            'controller_name' => 'EmailReadController',
        ]);
    }
}
