<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ChartPieController extends AbstractController
{
    #[Route('/chart-pie', name: 'app_chart_pie')]
    public function index(): Response
    {
        return $this->render('chart_pie/index.html.twig', [
            'controller_name' => 'ChartPieController',
        ]);
    }
}
