<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OffcanvasController extends AbstractController
{
    #[Route('/offcanvas', name: 'app_offcanvas')]
    public function index(): Response
    {
        return $this->render('offcanvas/index.html.twig', [
            'controller_name' => 'OffcanvasController',
        ]);
    }
}
