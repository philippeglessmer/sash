<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LoadersController extends AbstractController
{
    #[Route('/loaders', name: 'app_loaders')]
    public function index(): Response
    {
        return $this->render('loaders/index.html.twig', [
            'controller_name' => 'LoadersController',
        ]);
    }
}
