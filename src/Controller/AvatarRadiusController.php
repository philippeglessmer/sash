<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AvatarRadiusController extends AbstractController
{
    #[Route('/avatar-radius', name: 'app_avatar_radius')]
    public function index(): Response
    {
        return $this->render('avatar_radius/index.html.twig', [
            'controller_name' => 'AvatarRadiusController',
        ]);
    }
}
