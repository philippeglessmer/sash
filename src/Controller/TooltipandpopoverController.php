<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TooltipandpopoverController extends AbstractController
{
    #[Route('/tooltipandpopover', name: 'app_tooltipandpopover')]
    public function index(): Response
    {
        return $this->render('tooltipandpopover/index.html.twig', [
            'controller_name' => 'TooltipandpopoverController',
        ]);
    }
}
