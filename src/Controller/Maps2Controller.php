<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Maps2Controller extends AbstractController
{
    #[Route('/maps2', name: 'app_maps2')]
    public function index(): Response
    {
        return $this->render('maps2/index.html.twig', [
            'controller_name' => 'Maps2Controller',
        ]);
    }
}
