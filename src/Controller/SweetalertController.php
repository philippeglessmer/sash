<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SweetalertController extends AbstractController
{
    #[Route('/sweetalert', name: 'app_sweetalert')]
    public function index(): Response
    {
        return $this->render('sweetalert/index.html.twig', [
            'controller_name' => 'SweetalertController',
        ]);
    }
}
