<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ChartChartistController extends AbstractController
{
    #[Route('/chart-chartist', name: 'app_chart_chartist')]
    public function index(): Response
    {
        return $this->render('chart_chartist/index.html.twig', [
            'controller_name' => 'ChartChartistController',
        ]);
    }
}
