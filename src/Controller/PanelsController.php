<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PanelsController extends AbstractController
{
    #[Route('/panels', name: 'app_panels')]
    public function index(): Response
    {
        return $this->render('panels/index.html.twig', [
            'controller_name' => 'PanelsController',
        ]);
    }
}
