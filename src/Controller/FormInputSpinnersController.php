<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FormInputSpinnersController extends AbstractController
{
    #[Route('/form-input-spinners', name: 'app_form_input_spinners')]
    public function index(): Response
    {
        return $this->render('form_input_spinners/index.html.twig', [
            'controller_name' => 'FormInputSpinnersController',
        ]);
    }
}
