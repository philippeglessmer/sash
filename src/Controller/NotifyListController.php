<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NotifyListController extends AbstractController
{
    #[Route('/notify-list', name: 'app_notify_list')]
    public function index(): Response
    {
        return $this->render('notify_list/index.html.twig', [
            'controller_name' => 'NotifyListController',
        ]);
    }
}
