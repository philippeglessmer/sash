<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EditTableController extends AbstractController
{
    #[Route('/edit-table', name: 'app_edit_table')]
    public function index(): Response
    {
        return $this->render('edit_table/index.html.twig', [
            'controller_name' => 'EditTableController',
        ]);
    }
}
