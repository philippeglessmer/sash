<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ExtensionTablesController extends AbstractController
{
    #[Route('/extensio-tables', name: 'app_extension_tables')]
    public function index(): Response
    {
        return $this->render('extension_tables/index.html.twig', [
            'controller_name' => 'ExtensionTablesController',
        ]);
    }
}
