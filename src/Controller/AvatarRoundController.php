<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AvatarRoundController extends AbstractController
{
    #[Route('/avatar-round', name: 'app_avatar_round')]
    public function index(): Response
    {
        return $this->render('avatar_round/index.html.twig', [
            'controller_name' => 'AvatarRoundController',
        ]);
    }
}
