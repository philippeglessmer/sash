<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FilemanagerDetailsController extends AbstractController
{
    #[Route('/filemanager-details', name: 'app_filemanager_details')]
    public function index(): Response
    {
        return $this->render('filemanager_details/index.html.twig', [
            'controller_name' => 'FilemanagerDetailsController',
        ]);
    }
}
